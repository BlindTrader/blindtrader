const usernameSelector_ = '#container > div > div > div > form > div.uppercase.su-input-group > input[type=text]'
const passwordSelector_ = '#container > div > div > div > form > div:nth-child(3) > input[type=password]'
const loginPageButtonSelector_ = '#container > div > div > div > form > div.actions > button'
const pinSelector_ = '#container > div > div > div > form > div:nth-child(2) > div > input[type=password]'
const pinSubmitSelector_ = '#container > div > div > div > form > div.actions > button'
const firstStockSelector_ = '#app > div.container.wrapper > div.container-left > div > div.instruments > div > div.vddl-draggable.instrument.up.index0 > div > div > span.symbol > span > span'
const niceElementPartOne_ = '#app > div.container.wrapper > div.container-left > div > div.instruments > div > div.vddl-draggable.instrument.index'
const niceElementPartTwo_ = '#app > div.container.wrapper > div.container-left > div > div.instruments > div > div.vddl-draggable.instrument.index1 > div > div > span.symbol > span > span'
const ordersPageButton_ = '#app > div.container.wrapper > div.container-right > div.page-content.orders > div > div > button'
const ordersPageText_ = '#app > div.modal-mask.market-depth-widget > div > div > div.modal-header > div > div > div > div > div > input[type=text]'

module.exports = {
  usernameSelector_,
  passwordSelector_,
  loginPageButtonSelector_,
  pinSelector_,
  pinSubmitSelector_,
  firstStockSelector_,
  niceElementPartOne_,
  niceElementPartTwo_,
  ordersPageButton_,
  ordersPageText_
}
