# BlindTrader

A simple scraper based on puppeteer to subert the expensive API offered by Zerodha

![sjs_sticker](https://raw.githubusercontent.com/standard/standard/master/badge.png)

## Description

Uses a headless chrome browser to scrape data off an Indian stock exchange broker

## Getting Started

### Dependencies

* NodeJS 12+

### Installing

* `npm install`
* Add your Zerodha credentials into config/creds_.js (you can use an example config file from creds_sample.js to make this file)

It won't run in terminal shells by default because it runs with headless false, change the `headless` flag in package.json to `true` to start in headless mode.

### Executing program

* `npm start`

It takes a while to login, it's NOT stuck. It just waits for all elements in the page to be loaded.

## Help

Raise an issue, if you're not annoying I'll try to respond

## Version History

* 0.0.1
    * Initial Release

## License

This project is licensed under the MIT License - see the LICENSE.md file for details