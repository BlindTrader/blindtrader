'use strict'
const puppeteer = require('puppeteer')

const creds_ = require('./config/creds_.js')
const login_ = require('./config/login_.js')
const pjson = require('./package.json')

const MongoClient = require('mongodb').MongoClient
const uri = 'mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false'
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true })
client.connect(err => {
  const collection = client.db('BlindTrader').collection('trading_data');

  (async () => {
    const browser = await puppeteer.launch({
      headless: pjson.headless,
      defaultViewport: null,
      args: ['--disable-notifications']
    })

    const page = await browser.newPage()
    await page.setViewport({ width: 1366, height: 768 })

    await page.setDefaultNavigationTimeout(0)
    await page.goto('https://kite.zerodha.com/', {
      waitUntil: 'networkidle2',
      timeout: 3000000
    })

    /* Login to Zerodha */
    await page.waitFor(6000)

    await page.click(login_.usernameSelector_)
    await page.keyboard.type(creds_.username)
    await page.click(login_.passwordSelector_)
    await page.keyboard.type(creds_.password)

    await page.click(login_.loginPageButtonSelector_) /* Login Button Click */
    await page.waitFor(6000)

    await page.click(login_.pinSelector_)
    await page.keyboard.type(creds_.pin)

    await page.click(login_.pinSubmitSelector_) /* Pin Submit Button Click */
    await page.waitFor(20000)

    await page.waitForSelector('#app > div.container.wrapper > div > div > div > h1 > span')
    await page.waitFor(6000)

    while (true) {
      await page.keyboard.press('ArrowDown')
      await page.keyboard.press('D')
      await page.waitFor(500)
      await page.keyboard.press('D')

      const ticksStorage_ = await page.evaluate(() => {
        const session = localStorage.getItem('__storejs_kite_ticker/ticks')
        return session
      })

      var jsonStockData_ = JSON.parse(ticksStorage_.replace(/'/g, '"'))

      Object.entries(jsonStockData_).forEach(([key, value]) => {
        if (value.depth !== undefined && value.mode === 'full') {
          value.lastTradedTime = Date.now()
          collection.findOne(value, function (err, docs) {
            if (err) throw err
            if (docs === null) {
              collection.insertOne(value, function (err, res) {
                if (err) throw err
                console.log('One entry added for ' + key + ' stock.')
              })
            } else {
              console.log('The last data for stock ' + key + ' has not changed. (' + value.lastTradedTime + ')')
            }
          })
        }
      })
    }
  })()

  if (err) {
    console.log(err)
  }
})
